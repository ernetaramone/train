<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/', 'ArticleController@home');



Route::get('/profile', 'PublicController@profile');

Route::post('/profile', 'MeasurementController@storedata');


Route::get('/logout','\App\Http\Controllers\Auth\LoginController@logout');


Route::get('/calendar', 'TasksController@index');



Route::get('/json-date','PublicController@autoload');



Route::resource('tasks', 'TasksController');


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/admin', 'AdminController@admin')
    ->middleware('is_admin')
    ->name('admin');
Route::post('/admin', 'AdminController@storeArticles');

Route::get('/admin/{post}/delete', 'AdminController@deletePost');
Route::get('/admin/{client}/delete', 'AdminController@deleteUser');

Route::get('/{post}/edit', 'AdminController@goEditArcticle');


Route::get('/full/{post}', 'ArticleController@showFull');

Route::get('/{client}/client-date', 'AdminController@goClient');
Route::get('/{client}/date', 'AdminController@goDate');
Route::get('/{time}/edit-task', 'AdminController@goEditTask');

Route::patch('/{post}', 'AdminController@update');
Route::patch('/{time}/edit-task', 'AdminController@updateTaskVer');

Route::patch('/{client}/date', 'AdminController@updateClients');
Route::get('/{client}/add', 'AdminController@goAdd');
Route::post('/{client}/add', 'AdminController@adminAddMea');

