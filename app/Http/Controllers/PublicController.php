<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Measurement;
use Illuminate\Support\Facades\Input;




use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function profile()
    {
        $notes = array('Pagerėjo', 'Nepasikeitė ', 'Pablogėjo', );
        $records = Measurement::where('user_id', '=', Auth::id())->get();
        $pro = array('weight', 'weight_Note', 'yForward_L', 'yForward_R', 'yForward_Note', 'ySide_L', 'ySide_R', 'ySide_Note', 'yBack_L', 'yBack_R', 'yBack_Note', 'pushUp', 'pushUp_Note',
            'sitUp', 'sitUp_Note', 'plank', 'plank_Note', 'back', 'back_Note', 'shoulders_L', 'shoulders_R', 'shoulders_Note', 'jump', 'jump_Note', 'hr', 'hr_Note', 'comments', 'comments_Note');

        $names = DB::table('users')
            ->where('id', Auth::id())
            ->get();

        return view('pages.profile', compact('names', 'pro', 'records', 'notes'));
    }


    public function autoload()
    {
        $date = Input::get('date_id');
        $dateCheck = Measurement::where('date', '=', $date)
            ->where('user_id', '=', Auth::id())
            ->get();
        return response()->json($dateCheck);
    }


}
