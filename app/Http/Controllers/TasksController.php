<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;

use Illuminate\Http\Request;
use App\Task;

class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tasks = Task::all();
        $id =  User::where('id', '=', Auth::id())->get();
        return view('pages.calendar', compact('tasks', 'id'));
    }

    public function create()
    {
        return view('tasks.create');
    }

    public function store(Request $request)
    {
            Task::create([
                'workout' => request('workout'),
                'task_date' => request('task_date'),
                'ver' => request('ver'),
                'user_id' => auth()->id(),

            ]);
            return back();
    }
//    public function edit($id)
//    {
//        $info= Task::where('user_id', '=', Auth::id())->get();
//        return view('pages.edit', compact( 'id', 'info'));
//    }


}
