<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\article;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts=DB::table('articles')->orderBy('id', 'desc')->paginate(3);

        return view('pages.home', compact('posts'));
    }
}
