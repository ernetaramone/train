<?php

namespace App\Http\Controllers;
use App\article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Carbon;
use App\User;

use App\Measurement;

class ArticleController extends Controller
{
//    public function home()
//    {
//        return view('pages.home');
//    }
    public function showFull(article $post)
    {
        return view('pages.full', compact('post'));
    }
}


