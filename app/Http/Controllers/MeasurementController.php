<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Measurement;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class MeasurementController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
    public function storedata ()
    {
        $dateCheck = Measurement::where('user_id', '=',Auth::id())
            ->where('date', '=', Input::get('date'))->first();
        if ($dateCheck === null) {

            Measurement::create([
                'date' => request('date'),
                'weight' => request('weight'),
                'weight_Note' => request('weight_Note'),
                'yForward_L' => request('yForward_L'),
                'yForward_R' => request('yForward_R'),
                'yForward_Note' => request('yForward_Note'),
                'ySide_L' => request('ySide_L'),
                'ySide_R' => request('ySide_R'),
                'ySide_Note' => request('ySide_Note'),
                'yBack_L' => request('yBack_L'),
                'yBack_R' => request('yBack_R'),
                'yBack_Note' => request('yBack_Note'),
                'pushUp' => request('pushUp'),
                'pushUp_Note' => request('pushUp_Note'),
                'sitUp' => request('sitUp'),
                'sitUp_Note' => request('sitUp_Note'),
                'plank' => request('plank'),
                'plank_Note' => request('plank_Note'),
                'back' => request('back'),
                'back_Note' => request('back_Note'),
                'shoulders_L' => request('shoulders_L'),
                'shoulders_R' => request('shoulders_R'),
                'shoulders_Note' => request('shoulders_Note'),
                'jump' => request('jump'),
                'jump_Note' => request('jump_Note'),
                'hr' => request('hr'),
                'hr_Note' => request('hr_Note'),
                'comments' => request('comments'),
                'comments_Note' => request('comments_Note'),
                'user_id' => auth()->id(),

            ]);
            return back();
        }
        return back();

    }
}