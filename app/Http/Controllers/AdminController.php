<?php

namespace App\Http\Controllers;
use App\article;
use App\measurement;
use App\User;
use App\Task;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function admin()
    {
        $posts =DB::table('articles')->orderBy('id', 'desc')->paginate(10);
        $clients =DB::table('users')->where('type', "=","default")->orderBy('name')->paginate(12);
        $times =DB::table('tasks')->orderBy('task_date', 'desc')->paginate(12);

        return view('pages.admin', compact('posts', 'clients', 'times'));
    }
    public function storeArticles(Request $request)
    {
        article::create([
            'title' => request('title'),
            'body' => request('body'),
        ]);
        return back();
    }
    public function deletePost(article $post)
    {
        $post->delete();
        return redirect('/admin');
    }
    public function deleteUser(User $client)
    {
        $client->delete();
        return redirect('/admin');
    }
    public function goClient(Measurement $client)
    {
        $clients =DB::table('measurements')->where('user_id', '=', $client->id)->orderBy('date')->paginate(12);

        return view('pages.client-date', compact('client', 'clients'));
    }
    public function goDate(Measurement $client )
    {
        return view('pages.date', compact('client'));
    }
    public function goEditArcticle(article $post)
    {
        return view('pages.edit', compact('post'));
    }

    public function goAdd(Measurement $client)
    {
        $notes = array('Pagerėjo', 'Nepasikeitė ', 'Pablogėjo', );

        return view('pages.add', compact('client', 'notes'));
    }
    public function goEditTask(Task $time)
    {
        return view('pages.edit-task', compact('time'));
    }
    public function update(Request $request, article $post)
    {
        article::where('id', $post->id)->update($request->only(['title', 'body',]));

        return redirect('/admin');
    }
    public function updateClients(Request $request, Measurement $client)
    {
        Measurement::where('id', $client->id)->update($request->only(['date', 'user_id', 'weight',  'weight_Note', 'yForward_L', 'yForward_R', 'yForward_Note', 'ySide_L', 'ySide_R', 'ySide_Note', 'yBack_L', 'yBack_R', 'yBack_Note', 'pushUp', 'pushUp_Note',
            'sitUp', 'sitUp_Note', 'plank', 'plank_Note', 'back', 'back_Note', 'shoulders_L', 'shoulders_R', 'shoulders_Note', 'jump', 'jump_Note', 'hr', 'hr_Note', 'comments', 'comments_Note'
        ]));
        return redirect('/admin');
    }
    public function updateTaskVer(Request $request, Task $time)
    {
        Task::where('id', $time->id)->update($request->only([ 'task_date']));

        return redirect('/admin');
    }

    public function adminAddMea( Measurement $client)
    {
        $dateCheck = Measurement::where('user_id', '=', $client->id)
            ->where('date', '=', Input::get('date'))->first();
        if ($dateCheck === null) {

            Measurement::create([
                'date' => request('date'),
                'weight' => request('weight'),
                'weight_Note' => request('weight_Note'),
                'yForward_L' => request('yForward_L'),
                'yForward_R' => request('yForward_R'),
                'yForward_Note' => request('yForward_Note'),
                'ySide_L' => request('ySide_L'),
                'ySide_R' => request('ySide_R'),
                'ySide_Note' => request('ySide_Note'),
                'yBack_L' => request('yBack_L'),
                'yBack_R' => request('yBack_R'),
                'yBack_Note' => request('yBack_Note'),
                'pushUp' => request('pushUp'),
                'pushUp_Note' => request('pushUp_Note'),
                'sitUp' => request('sitUp'),
                'sitUp_Note' => request('sitUp_Note'),
                'plank' => request('plank'),
                'plank_Note' => request('plank_Note'),
                'back' => request('back'),
                'back_Note' => request('back_Note'),
                'shoulders_L' => request('shoulders_L'),
                'shoulders_R' => request('shoulders_R'),
                'shoulders_Note' => request('shoulders_Note'),
                'jump' => request('jump'),
                'jump_Note' => request('jump_Note'),
                'hr' => request('hr'),
                'hr_Note' => request('hr_Note'),
                'comments' => request('comments'),
                'comments_Note' => request('comments_Note'),
                'user_id' => $client->id,

            ]);
            return redirect('/admin');        }
        return back();
    }

}