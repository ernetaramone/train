<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class measurement extends Model
{


    protected $fillable = [
        'date', 'user_id', 'weight',  'weight_Note', 'yForward_L', 'yForward_R', 'yForward_Note', 'ySide_L', 'ySide_R', 'ySide_Note', 'yBack_L', 'yBack_R', 'yBack_Note', 'pushUp', 'pushUp_Note',
        'sitUp', 'sitUp_Note', 'plank', 'plank_Note', 'back', 'back_Note', 'shoulders_L', 'shoulders_R', 'shoulders_Note', 'jump', 'jump_Note', 'hr', 'hr_Note', 'comments', 'comments_Note'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
