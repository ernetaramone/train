<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Šie duomenys nesutampa su mūsų turimais, bandykite dar kartą',
    'throttle' => 'Pper daug bandymų prisijungti, pabandykite po :seconds sekundžių.',

];
