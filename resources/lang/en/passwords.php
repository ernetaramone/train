<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Slaptažodis turi būti bent iš 6 simbolių ir sutapti',
    'reset' => 'Jūsų slaptažodis pakeistas',
    'sent' => 'Slaptažodžio pakeitimo duomenis išsiuntėme Jums į el. paštą',
    'token' => 'This password reset token is invalid.',
    'user' => "Vartotojo su tokiu el. paštu mūsų sitemoje nėra.",

];
