<!DOCTYPE html>
<html>

<body>

@include('includes/head')
<section class="ar-admin admin" id="full-articles-admin">
    <div class="container">
        <form action="/{{$post->id}}" method="post" class="form-horizontal">
            {{csrf_field()}}
            {{method_field('PATCH')}}

            <div class="tab-admin">
                <div class="item-a "><h3>Pavadinimas</h3></div>
                <div class="item-a "><h3>Tekstas</h3></div>
                <div class="item-a "><h3>Redaguoti</h3></div>
                <div class="item-a "><h3></h3></div>

                <div class="item-a "><textarea required type="text" name="title" id="title" required >{{$post->title}}</textarea></div>
                <div class="item-a " ><textarea required type="text" name="body" id="body" required >{{$post->title}}</textarea></div>
                <div class="item-a " ><button class="btn btn-warning" type="submit" name="submit" value="submit">Išsaugoti</button></div>
                <div class="item-a "><p> <a class="btn btn-danger" href="/admin" role="button"><i class="fa fa-close"></i>Atšaukti</a></p></div>

            </div>
        </form>
    </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="js/js.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>


</body>
</html>
