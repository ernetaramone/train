<!DOCTYPE html>
<html>

<body>

@include('includes/head')

<section class="profile-tbl">
    <div class="container profile">
        <form action="/{{$client->id}}/date" method="post" class="form-horizontal">
            {{csrf_field()}}

            {{method_field('PATCH')}}

            <div class="tab2 admin-pro">

                <div class="item nscr item01"> </div>

                <div class="item scr item1r r">Rezultatas</div>
                <div class="item scr item1 r">Pastaba</div>

                <div class="item nscr item01 l01 r">Data</div>


                <div class="item scr itemx3 l01 show">
                    <textarea  type="text" name="date" id="date" required style="background:transparent; border-color: transparent;">{{$client->date}}</textarea></div>

                <div class="item nscr item02 l02 r">Svoris</div>

                <div class="item scr item1r l02 show">
                    <textarea  type="text" name="weight" id="weight" required style="background:transparent; border-color: transparent;">{{$client->weight}}</textarea></div>
                <div class="item scr item1 l02 show">
                    <textarea  type="text" name="weight_Note" id="weight_Note" required style="background:transparent; border-color: transparent;">{{$client->weight_Note}}</textarea></div>

                <div class="item nscr itemy  yl03 r">Y </br> Balan</br>savimas</div>
                <div class="item nscr item1 l03"> </div>

                <div class="item scr item1 l03 r">Kairė </div>
                <div class="item scr item1 l03 r">Dešinė </div>
                <div class="item scr item1 l03"  > </div>

                <div class="item nscr item1 l04 r">Tiesiai</div>

                <div class="item scr item1 show l04">
                    <textarea  type="text" name="yForward_L" id="yForward_L" required style="background:transparent; border-color: transparent;">{{$client->yForward_L}}</textarea></div>
                <div class="item scr item1 show l04">
                    <textarea  type="text" name="yForward_R" id="yForward_R" required style="background:transparent; border-color: transparent;">{{$client->yForward_R}}</textarea></div>
                <div class="item scr item1 show l04">
                    <textarea  type="text" name="yForward_Note" id="yForward_Note" required style="background:transparent; border-color: transparent;">{{$client->yForward_Note}}</textarea></div>

                <div class="item nscr item1 l05 r">Į šoną tiesti, koja atgal</div>

                <div class="item scr item1 show l05">
                    <textarea  type="text" name="ySide_L" id="ySide_L" required style="background:transparent; border-color: transparent;">{{$client->ySide_L}}</textarea></div>


                <div class="item scr item1 show l05">
                    <textarea  type="text" name="ySide_R" id="ySide_R" required style="background:transparent; border-color: transparent;">{{$client->ySide_R}}</textarea></div>


                <div class="item scr item1 show l05">
                    <textarea  type="text" name="ySide_Note" id="ySide_Note" required style="background:transparent; border-color: transparent;">{{$client->ySide_Note}}</textarea></div>




                <div class="item nscr item1 l06 r">Užsukta koja atgal</div>

                <div class="item scr item1 show l06">
                    <textarea  type="text" name="yBack_L" id="yBack_L" required style="background:transparent; border-color: transparent;">{{$client->yBack_L}}</textarea></div>


                <div class="item scr item1 show l06">
                    <textarea  type="text" name="yBack_R" id="yBack_R" required style="background:transparent; border-color: transparent;">{{$client->yBack_R}}</textarea></div>


                <div class="item scr item1 show l06">
                    <textarea  type="text" name="yBack_Note" id="yBack_Note" required style="background:transparent; border-color: transparent;">{{$client->yBack_Note}}</textarea></div>




                <div class="item nscr item02 l07 r">Atsispaudimai</div>

                <div class="item scr item00 show l07">
                    <textarea  type="text" name="pushUp" id="pushUp" required style="background:transparent; border-color: transparent;">{{$client->pushUp}}</textarea></div>

                <div class="item scr item1 show l07">
                    <textarea  type="text" name="pushUp_Note" id="pushUp_Note" required style="background:transparent; border-color: transparent;">{{$client->pushUp_Note}}</textarea></div>


                <div class="item nscr item02 l08 r">Atsilenkimai</div>

                <div class="item scr item00 show l08">
                    <textarea  type="text" name="sitUp" id="sitUp" required style="background:transparent; border-color: transparent;">{{$client->sitUp}}</textarea></div>



                <div class="item scr item1 show l08">
                    <textarea  type="text" name="sitUp_Note" id="sitUp_Note" required style="background:transparent; border-color: transparent;">{{$client->sitUp_Note}}</textarea></div>






                <div class="item nscr item02 l09 r">Plank (lenta)</div>

                <div class="item scr item00 show l09">
                    <textarea  type="text" name="plank" id="plank" required style="background:transparent; border-color: transparent;">{{$client->plank}}</textarea></div>



                <div class="item scr item1 show l09">
                    <textarea  type="text" name="plank_Note" id="plank_Note" required style="background:transparent; border-color: transparent;">{{$client->plank_Note}}</textarea></div>



                <div class="item nscr item02 l10 r">Nugaros atkėlimas su atrama</div>

                <div class="item scr item00 show l10">
                    <textarea  type="text" name="back" id="back" required style="background:transparent; border-color: transparent;">{{$client->back}}</textarea></div>

                <div class="item scr item1 show l10">
                    <textarea  type="text" name="back_Note" id="back_Note" required style="background:transparent; border-color: transparent;">{{$client->back_Note}}</textarea></div>


                <div class="item nscr item02 l11 r">Pečių mobilumo testas</div>

                <div class="item scr item1 show l11">
                    <textarea  type="text" name="shoulders_L" id="shoulders_L" required style="background:transparent; border-color: transparent;">{{$client->shoulders_L}}</textarea></div>

                <div class="item scr item1 show l11">
                    <textarea  type="text" name="shoulders_R" id="shoulders_R" required style="background:transparent; border-color: transparent;">{{$client->shoulders_R}}</textarea></div>

                <div class="item scr item1 show l11">
                    <textarea  type="text" name="shoulders_Note" id="shoulders_Note" required style="background:transparent; border-color: transparent;">{{$client->shoulders_Note}}</textarea></div>



                <div class="item nscr item02 l12 r">Šuolis į auktšti iš vietos</div>

                <div class="item scr item00 show l12">
                    <textarea  type="text" name="jump" id="jump" required style="background:transparent; border-color: transparent;">{{$client->jump}}</textarea></div>

                <div class="item scr item1 show l12">
                    <textarea  type="text" name="jump_Note" id="jump_Note" required style="background:transparent; border-color: transparent;">{{$client->jump_Note}}</textarea></div>



                <div class="item nscr item02 l13 r">SSD Atsigavimas</div>

                <div class="item scr item00 show l13">
                    <textarea  type="text" name="title" id="hr" required style="background:transparent; border-color: transparent;">{{$client->hr}}</textarea></div>

                <div class="item scr item1 show l13">
                    <textarea  type="text" name="title" id="hr_Note" required style="background:transparent; border-color: transparent;">{{$client->hr_Note}}</textarea></div>



                <div class="item nscr item02 l14 r">Komentarai</div>

                <div class="item scr item00 show l14">
                    <textarea  type="text" name="comments" id="comments" required style="background:transparent; border-color: transparent;">{{$client->comments}}</textarea></div>
                <div class="item scr item1 show l14">
                    <textarea  type="text" name="comments_Note" id="comments_Note" required style="background:transparent; border-color: transparent;">{{$client->comments_Note}}</textarea></div>



                <div>

                <div class="item nscr item02 h"> </div>
                <div class="item scr item00 h"> </div>
            </div>
            </div>
            <div class="item-a " ><button class="btn btn-warning" type="submit" name="submit" value="submit">Išsaugoti</button></div>

            <div class="item-a "><p> <a class="btn btn-primary" href="/admin" role="button"><i class="fa fa-close"></i>Grįžti atgal</a></p></div>

        </form>
    </div>

</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="js/js.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>


</body>
</html>
