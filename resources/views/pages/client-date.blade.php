<!DOCTYPE html>
<html>

<body>

@include('includes/head')
<section class="ar-admin admin" id="full-articles-admin">
    <div class="container">

        <form action="/{{$client->id}}" method="post" class="form-horizontal">
            {{csrf_field()}}
            {{method_field('PATCH')}}

            <div class="tab-admin">
                <div class="item-a items-s "><h3>Data</h3></div>

                <div class="item-a  items-s "><h3>Žiūrėti</h3></div>
                @foreach($clients as $client)

                <div class="item-a items-s "><p>{{$client->date}}</p></div>

                    <div class="item-a items-s  "><p><a class="btn btn-warning"  href="/{{$client->id}}/date"  role="button">Žiūrėti</a></p></div>
                @endforeach

            </div>

        </form>
        <div class="center">
            <div class="center-c">
                {{$clients->links()}}
            </div>
        </div>

    </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="js/js.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>


</body>
</html>
