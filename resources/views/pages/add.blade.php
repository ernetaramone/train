<!DOCTYPE html>
<html>
@include('includes/head')
<script src="{{ asset('jquery/datepicker-lt.js') }}" type='text/javascript'></script>

<body>



<section class="profile-tbl">
    <div class="container profile">
        <form action="/{{$client->id}}/add" method="post">
            {{csrf_field()}}
            <div class="tab">
                <div class="item nscrz show item01"> </div>
                <div class="item scr item1r r">Rezultatas</div>
                <div class="item scr item1 r">Pastaba</div>

                <div class="item nscr item01 l01 r">Data</div>
                <div class="item scr itemx3 l01" >
                    <input id="date"   name="date" autofocus required contenteditable="false" />

                </div>


                <div class="item nscr item02 l02 r">Svoris</div>
                <div class="item scr item1r l02"> <input id="weight" type="number"   name="weight" required  />
                </div>
                <div class="item scr item1 l02" >
                    <select   name="weight_Note" id="weight_Note">
                        @foreach ($notes as $note)
                            <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="item nscr itemy  yl03 r">Y </br> Balansavimas</div>
                <div class="item nscr show item1 l03"> </div>
                <div class="item scr item1 l03 r">Kairė </div>
                <div class="item scr item1 l03 r">Dešinė </div>
                <div class="item scr  show item1 l03"  > </div>


                <div class="item nscr item1 l04 r">Tiesiai</div>
                <div class="item scr item1 l04"> <input id="yForward_L" type="number"   name="yForward_L" required/>
                </div>
                <div class="item scr item1 l04" > <input id="yForward_R" type="number"   name="yForward_R" required />
                </div>
                <div class="item scr item1 l04">
                    <select   name="yForward_Note" id="yForward_Note">
                        @foreach ($notes as $note)
                            <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="item nscr item1 l05 r">Į šoną tiesti, koja atgal</div>
                <div class="item scr item1 l05"><input id="ySide_L" type="number"   name="ySide_L" required /> </div>
                <div class="item scr item1 l05"> <input id="ySide_R" type="number"   name="ySide_R" required /></div>
                <div class="item scr item1 l05">
                    <select   name="ySide_Note" id="ySide_Note">
                        @foreach ($notes as $note)
                            <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                        @endforeach
                    </select>
                </div>


                <div class="item nscr item1 l06 r">Užsukta koja atgal</div>
                <div class="item scr item1 l06"> <input id="yBack_L" type="number"   name="yBack_L" required/></div>
                <div class="item scr item1 l06"> <input id="yBack_R" type="number"   name="yBack_R"  required/></div>
                <div class="item scr item1 l06">
                    <select   name="yBack_Note" id="yBack_Note">
                        @foreach ($notes as $note)
                            <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                        @endforeach
                    </select>
                </div>


                <div class="item nscr item02 l07 r">Atsispaudimai</div>
                <div class="item scr item00 l07"> <input id="pushUp" type="number"   name="pushUp"  required/></div>
                <div class="item scr item1 l07">
                    <select   name="pushUp_Note" id="pushUp_Note">
                        @foreach ($notes as $note)
                            <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="item nscr item02 l08 r">Atsilenkimai</div>
                <div class="item scr item00 l08">  <input id="sitUp" type="number"   name="sitUp" required/></div>
                <div class="item scr item1 l08">
                    <select   name="sitUp_Note" id="sitUp_Note">
                        @foreach ($notes as $note)
                            <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="item nscr item02 l09 r">Plank (lenta)</div>
                <div class="item scr item00 l09"> <input id="plank" type="number"   name="plank"  required/></div>
                <div class="item scr item1 l09">
                    <select   name="plank_Note" id="plank_Note">
                        @foreach ($notes as $note)
                            <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="item nscr item02 l10 r">Nugaros atkėlimas su atrama</div>
                <div class="item scr item00 l10">  <input id="back" type="number"   name="back"  required/></div>
                <div class="item scr item1 l10">
                    <select   name="back_Note" id="back_Note">
                        @foreach ($notes as $note)
                            <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                        @endforeach
                    </select>
                </div>


                <div class="item nscr item02 l11 r">Pečių mobilumo testas</div>
                <div class="item scr item1 l11"> <input id="shoulders_L" type="number"   name="shoulders_L"  required/></div>
                <div class="item scr item1 l11"> <input id="shoulders_R" type="number"   name="shoulders_R" required /></div>
                <div class="item scr item1 l11">
                    <select   name="shoulders_Note" id="shoulders_Note">
                        @foreach ($notes as $note)
                            <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="item nscr item02 l12 r">Šuolis į auktšti iš vietos</div>
                <div class="item scr item00 l12"> <input id="jump" type="number"   name="jump"  required/></div>
                <div class="item scr item1 l12">
                    <select   name="jump_Note" id="jump_Note">
                        @foreach ($notes as $note)
                            <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="item nscr item02 l13 r">SSD Atsigavimas</div>
                <div class="item scr item00 l13"> <input id="hr" type="number"   name="hr"  required/> </div>
                <div class="item scr item1 l13">
                    <select   name="hr_Note" id="hr_Note">
                        @foreach ($notes as $note)
                            <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                        @endforeach
                    </select>
                </div>


                <div class="item nscr item02 l14 r">Komentarai</div>
                <div class="item scr item00 l14"> <input id="comments" type="number"   name="comments"   required/> </div>
                <div class="item scr item1 l14"> <input id="comments_Note" type="text"   name="comments_Note" required  />
                </div>


                <div class="item nscr item02 h"></div>
                <div class="item scr item00 h"> </div>
                <div class=" hh"> <button type="submit" name="submit" value="submit" class="btn ">Įrašyti</button></div>

            </div>
        </form>
    </div>
</section>

<script>
    $("#date").datepicker( $.datepicker.regional[ "lt" ] );

    document.addEventListener("DOMContentLoaded", function() {
        var elements = document.getElementsByTagName("INPUT");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Laukas negali būti tuščias");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    })
</script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="js/js.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>


</body>
</html>
