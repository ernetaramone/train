<!DOCTYPE html>
<html>

<body>

@include('includes/head')
<!-- NAV menu -->
@include('includes/nav-empty')

<section class="articles-admin articles-full" id="articles-admin">

    <div class="container">
        <div class="row">
            <div class="col-md-12 ar-admin">
                <div class="well  well-sm">
                    <form  method="post">
                        {{csrf_field()}}

                        <fieldset>
                                <legend class="text-center header">{{$post->title}}</legend>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="c" ><p>{{$post->body}}</p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <a class="btn btn-primary" href="/" role="button"><i class="fa fa fa-caret-square-o-left"></i>Grįžti atgal</a>
                                        </div>
                                    </div>
                                </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('includes/footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="js/js.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>
