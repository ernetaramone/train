<!DOCTYPE html>
<html>

<body>

@include('includes/head')

<section class="articles-admin" id="articles-admin">
    <div>
        <a class="btn btn-warning"  href="{{ route('logout') }}" role="button">Atsijungti</a>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 ar-admin">
                <div class="well  well-sm">
                    <form class="form-horizontal" action="/admin" method="post" >
                        {{csrf_field()}}

                        <fieldset>
                            <legend class="text-center header">Naujas straipsnis</legend>


                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><label for="title">Pavadinimas</label></span>
                                <div class="col-md-8">
                                    <input required type="text" name="title" id="title" placeholder="Įrašykite pvadinimą" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><label for="title">Tekstas</label></span>
                                <div class="col-md-8">
                                    <textarea class="form-control" required type="text" name="body" id="body" placeholder="Įrašykite tekstą" rows="7"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">Skelbti</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ar-admin admin" id="full-articles-admin">
    <div class="container">

        <form  method="post">
            {{csrf_field()}}
            <div class="item-a "><h3>Straipsniai</h3></div>

            <div class="tab-admin">
                <div class="item-a "><h3>Pavadinimas</h3></div>
                <div class="item-a "><h3>Tekstas</h3></div>
                <div class="item-a "><h3>Redaguoti</h3></div>
                <div class="item-a "><h3>Trinti</h3></div>
                @foreach($posts as $post)
                    <div class="item-a "><p>{{str_limit($post->title,15)}}</p></div>
                    <div class="item-a " ><p>{{str_limit($post->body,50)}}</p></div>
                    <div class="item-a "><p><a class="btn btn-warning"  href="/{{$post->id}}/edit"  role="button">Redaguoti</a></p></div>
                    <div class="item-a "><p> <a class="btn btn-danger" onclick="return confirm('Ar tikrai norite ištrinti?')" href="/admin/{{$post->id}}/delete" role="button"><i class="fa fa-trash"></i>Trinti</a></p></div>
                @endforeach

            </div>

        </form>
        <div class="center">
            <div class="center-c">
                {{$posts->links()}}
            </div>
        </div>

    </div>
</section>

<section class="ar-admin admin" id="full-articles-admin">
    <div class="container">

        <form  method="post">
            {{csrf_field()}}
            <div class="item-a "><h3>Klientų paieška</h3></div>
            <div class="tab-admin">
                <div class="item-a "><h3>Vardas</h3></div>
                <div class="item-a "><h3>Pavardė</h3></div>
                <div class="item-a  "><h3>Žiūrėti/redaguoti</h3></div>
                <div class="item-a  "><h3>Pridėti</h3></div>

            @foreach($clients as $client)
                    <div class="item-a "><p>{{$client->name}}</p></div>
                    <div class="item-a " ><p>{{$client->surname}}</p></div>
                    <div class="item-a  "><p><a class="btn btn-warning"  href="/{{$client->id}}/client-date"  role="button">Žiūrėti</a></p></div>
                    <div class="item-a  "><p><a class="btn btn-primary"  href="/{{$client->id}}/add"  role="button">Pridėti</a></p></div>
                @endforeach

            </div>

        </form>
        <div class="center">
            <div class="center-c">
                {{$clients->links()}}
            </div>
        </div>

    </div>
</section>


<section class="ar-admin admin" id="full-articles-admin">
    <div class="container">

        <form  method="post">
            {{csrf_field()}}
            <div class="item-a "><h3>Užsiėmimų tvirtinimas</h3></div>

            <div class="tab-admin">
                <div class="item-a items-s "><h3>Data</h3></div>
                <div class="item-a "><h3>Užsiėmimas</h3></div>
                <div class="item-a  "><h3>Žiūrėti</h3></div>
                @foreach($times as $time)
                    <div class="item-a items-s "><p>{{$time->task_date}}</p></div>
                    <div class="item-a " ><p>{{$time->workout}}</p></div>
                    <div class="item-a  "><p><a class="btn btn-warning"  href="/{{$time->id}}/edit-task"  role="button">Žiūrėti</a></p></div>
                @endforeach

            </div>

        </form>
        <div class="center">
            <div class="center-c">
                {{$times->links()}}
            </div>
        </div>

    </div>
</section>







<script type="text/javascript">

    document.addEventListener("DOMContentLoaded", function() {
        var elements = document.getElementsByTagName("INPUT");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Laukas negali būti tuščias");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    })



</script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="js/js.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>


</body>
</html>
