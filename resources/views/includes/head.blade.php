<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://use.fontawesome.com/9ebd24cee4.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>

    <title>ATIV</title>
    <link href="https://fonts.googleapis.com/css?family=Cabin+Condensed|Fira+Sans|Fjalla+One|Maitree|Secular+One|Viga" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <script src="{{ asset('jquery/jquery-ui.js') }}"></script>
    <script src="{{ asset('jquery/jquery-ui.min.js') }}"></script>


    <link rel="stylesheet" href="{{ asset('jquery/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('jquery/jquery-ui.structure.css') }}">
    <link rel="stylesheet" href="{{ asset('jquery/jquery-ui.theme') }}">

    <script src='jquery/datepicker-lt.js' type='text/javascript'></script>


</head>