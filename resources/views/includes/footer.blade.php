<footer class="contacts" id="contacts">
    <h3>Susisiekite</h3>
    <div class="container sec-footer">
        <div class="row sec4 row-centered ">
            <div class="col-lg-4 col-md-4 col-sm-7 col-xs-12 cols col-centered ">
                <h2>Adresas:</h2>
                <p> <i class="fa fa-map-marker" aria-hidden="true"></i> Savanorių pr. 323, Kaunas</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-7 col-xs-12 cols colxx col-centered">
                <h2>Kontaktai:</h2>
                <p> <i class="fa fa-phone" aria-hidden="true"></i> 370 623 8547</p>
                <p> <i class="fa fa-envelope-o" aria-hidden="true"></i> sportuok@treneris.com</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-7 col-xs-12 cols col-centered fonts">
                <h2>Sekite mane: </h2>
                <a  href=" https://www.facebook.com/ignas.volungevicius.9" target="_blank">
                <p><i class="fa fa-facebook-square" aria-hidden="true"></i></p></a>
                <a  href=" https://www.facebook.com/ignas.volungevicius.9" target="_blank">
                <p><i class="fa fa-twitter-square" aria-hidden="true"></i></p></a>
                <a  href=" https://www.facebook.com/ignas.volungevicius.9" target="_blank">
                <p><i class="fa fa-instagram" aria-hidden="true"></i></p></a>
            </div>
        </div>
    </div>

    <iframe  src="https://www.google.com/maps/embed/v1/search?q=Savanori%C5%B3%20prospektas%20323%2C%20Kaunas%2C%20Lietuva&key=AIzaSyAFClO9wXz4XbNCQI9l8HEYKVGqv4IYvO8" allowfullscreen></iframe>

</footer>