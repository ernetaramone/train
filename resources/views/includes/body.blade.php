
<!-- First page-->
<section class="strt-page" id="strt-page">
    <div class="container">
        <!-- Quote -->
        <div class="row quo">
            <div class="col-lg-6 col-md-6 col-sm-8 quo-1">
                <p >Kuo sunkesnė pergalė, </p>
                <p>tuo didesnis laimėjimo džiaugsmas </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 auth-1">
                <p>- Pele </p>
            </div>
        </div>
        <!-- Trener -->
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 trener">
                <p> Ignas Volungevičius - asmeninis treneris </p>
                <a class="btn btn-primary"  href="#workouts" role="button">Rinktis treniruotę</a>
                <a class="btn btn-primary"  href="/calendar" role="button"><i class="fa fa fa-caret-square-o-left"></i>Rezervuoti laiką</a>
            </div>
        </div>
    </div>
</section>
<!-- End of first page-->

<!-- Second page -->
<section class="about" id="about">
    <div class="container sec2">
        <div class="row tren">
            <!-- Img-->
            <div class="col-lg-6 col-md-6 col-sm-6 ">
                <img src="img/per.jpg" class="img-responsive">
            </div>
            <!-- Description -->
            <div class="col-lg-6 col-md-6 col-sm-6  about-text">
                <p>Apie mane</p>
                <p>Ignas Volungevičius</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut scelerisque lacinia lacus, in volutpat lorem. In hac habitasse platea dictumst.
                    Quisque quis sapien nec neque feugiat molestie. Integer rutrum ante sit amet orci rutrum, id fermentum justo congue. Suspendisse eget iaculis orci.
                    Praesent facilisis vitae eros a gravida. Vestibulum sodales maximus erat at pellentesque. Proin venenatis ut urna ut ornare. Vivamus quis fringilla lectus.
                    Quisque mollis. Suspendisse potenti. Duis non gravida turpis.<br>
                    <br>Pellentesque volutpat, elit et dictum convallis, urna nulla sollicitudin metus, ut condimentum tortor mi vel urna. Nulla imperdiet lorem ut purus finibus, at
                    dignissim metus dictum.</p>
            </div>
        </div>
    </div>
</section>
<!-- End of second page -->

<!-- Third page-->
<section class="workouts" id="workouts">
    <div class="container">
        <div class="row align-items-center row-centered sec3">
            <!-- Workouts col -->
            <div class="col-lg-5 col-centered">
                <h2>Svorio metimas</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut scelerisque lacinia lacus, in volutpat lorem. In hac habitasse platea dictumst.
                    Quisque quis sapien nec neque feugiat molestie.</p>
            </div>
            <div class="col-lg-5 col-centered">
                <h2>Cardio</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut scelerisque lacinia lacus, in volutpat lorem. In hac habitasse platea dictumst.
                    Quisque quis sapien nec neque feugiat molestie.</p>
            </div>
            <div class="col-lg-5 col-centered">
                <h2>Kūno dizainas</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut scelerisque lacinia lacus, in volutpat lorem. In hac habitasse platea dictumst.
                    Quisque quis sapien nec neque feugiat molestie.</p>
            </div>
            <div class="col-lg-5 col-centered">
                <h2>Core</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut scelerisque lacinia lacus, in volutpat lorem. In hac habitasse platea dictumst.
                    Quisque quis sapien nec neque feugiat molestie.</p>
            </div>
        </div>
    </div>
</section>
<!-- End of third page-->