<nav>
    <div id="myNavbar" class=" navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="{{ url('/') }}">
                <p class="navbar-brand"> ATIV
                </a>
            </div>

            <div class="navbar-collapse collapse">


                    <!-- Nav menu pages -->
                <ul class="nav navbar-nav navbar-left">
                    <li class="nav-item active"> <a class=" nav-link scroll" href="#strt-page">Pradžia</a> </li>
                    <li class="nav-item"> <a class="nav-link scroll" href="#about">Apie</a></li>
                    <li class="nav-item"><a class="nav-link scroll" href="#workouts">Treniruotės</a> </li>
                    <li class="nav-item"><a class="nav-link scroll" href="#articles">Straipsniai</a></li>
                    <li class="nav-item"><a class="nav-link scroll" href="#contacts">Kontaktai</a></li>

                </ul>
                <!-- Register and Login buttons -->

                @if(Auth::check())

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/profile">Profilis</a></li>
                                <li><a href="/calendar">Užimtumo kalendorius</a></li>
                                <li><a href="{{ route('logout') }}">Atsijungti</a></li>

                            </ul>
                    </ul>
                @else
                <form class="navbar-form navbar-right" >

                    <li><a class="nav-link log" href="{{ route('login') }}">Prisijungti</a></li>
                    <li><a class="nav-link reg" href="{{ route('register') }}">Registruotis</a></li>
                </form>
                @endif

            </div>

        </div>
    </div>
</nav>
