<nav>
    <div id="myNavbar" class=" navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="{{ url('/') }}">
                <p class="navbar-brand"> ATIV
                </a>
            </div>

            <div class="navbar-collapse collapse">




                <!-- Register and Login buttons -->

                @if(Auth::check())

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/profile">Profilis</a></li>
                                <li><a href="/calendar">Užimtumo kalendorius</a></li>
                                <li><a href="{{ route('logout') }}">Atsijungti</a></li>

                            </ul>
                    </ul>
                @else
                <form class="navbar-form navbar-right">


                    <li><a class="nav-link" href="{{ route('login') }}">Prisijungti</a></li>
                    <li><a class="nav-link" href="{{ route('register') }}">Registruotis</a></li>
                </form>
                @endif

            </div>

        </div>
    </div>
</nav>
