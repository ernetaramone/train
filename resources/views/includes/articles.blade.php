
<!-- Fourth page -->
<section class="articles" id="articles">
    <h3>Naudinga žinoti</h3>

    <div class="container">
        <div class="row align-items-center row-centered sec5">
            <!-- Articles col -->
            @foreach($posts as $post)

            <div class="col-lg-12 col-md-12 col-sm-12 col  col-centered" >

                <h2>{{str_limit($post->title,25)}}</h2>
                <p>{{str_limit($post->body,300)}} </p>
                <p><a class="btn bt-info" href="/full/{{$post->id}}" role="button">Skaityti toliau &raquo;</a></p>
            </div>


               @endforeach
            <div >
                {{$posts->links()}}
            </div>


        </div>
    </div>
</section>
<!-- End of fourth page -->
