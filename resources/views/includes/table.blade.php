<section class="profile-p" >
    <div class="container adata">
        @foreach($names as $name)
        <a>{{$name->name}} {{$name->surname}}</a>
        <hr>
        <p>Fiziniai duomenys:</p>
        <p>Lytis: <a>{{$name->sex}}</a></p>
        <p>Amžius: <a>{{$name->age}} m</a> </p>
        <p>Ūgis: <a>{{$name->height}} cm</a> </p>
        <p>Tikslas: <a>{{$name->goal}}</a></p>
        @endforeach

    </div>
</section>
<section class="profile-tbl">
    <div class="container profile">
        <form action="/profile" method="post">
            {{csrf_field()}}
        <div class="tab">
        <div class="item nscrz show item01"> </div>
        <div class="item scr item1r r">Rezultatas</div>
        <div class="item scr item1 r">Pastaba</div>

        <div class="item nscr item01 l01 r">Data</div>
        <div class="item scr itemx3 l01" >
            <input id="date"   name="date" autofocus required contenteditable="false" />

        </div>


        <div class="item nscr item02 l02 r">Svoris</div>
        <div class="item scr item1r l02"> <input id="weight" type="number"   name="weight" required  />
        </div>
        <div class="item scr item1 l02" >
            <select   name="weight_Note" id="weight_Note">
                @foreach ($notes as $note)
                    <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                @endforeach
            </select>
        </div>

            <div class="item nscr itemy  yl03 r">Y </br> Balansavimas</div>
            <div class="item nscr show item1 l03"> </div>
            <div class="item scr item1 l03 r">Kairė </div>
            <div class="item scr item1 l03 r">Dešinė </div>
            <div class="item scr  show item1 l03"  > </div>


            <div class="item nscr item1 l04 r">Tiesiai</div>
            <div class="item scr item1 l04"> <input id="yForward_L" type="number"   name="yForward_L" required/>
            </div>
            <div class="item scr item1 l04" > <input id="yForward_R" type="number"   name="yForward_R" required />
            </div>
            <div class="item scr item1 l04">
                <select   name="yForward_Note" id="yForward_Note">
                    @foreach ($notes as $note)
                        <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                    @endforeach
                </select>
            </div>

            <div class="item nscr item1 l05 r">Į šoną tiesti, koja atgal</div>
            <div class="item scr item1 l05"><input id="ySide_L" type="number"   name="ySide_L" required /> </div>
            <div class="item scr item1 l05"> <input id="ySide_R" type="number"   name="ySide_R" required /></div>
            <div class="item scr item1 l05">
                <select   name="ySide_Note" id="ySide_Note">
                    @foreach ($notes as $note)
                        <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                    @endforeach
                </select>
            </div>


            <div class="item nscr item1 l06 r">Užsukta koja atgal</div>
            <div class="item scr item1 l06"> <input id="yBack_L" type="number"   name="yBack_L" required/></div>
            <div class="item scr item1 l06"> <input id="yBack_R" type="number"   name="yBack_R"  required/></div>
            <div class="item scr item1 l06">
                <select   name="yBack_Note" id="yBack_Note">
                    @foreach ($notes as $note)
                        <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                    @endforeach
                </select>
            </div>


            <div class="item nscr item02 l07 r">Atsispaudimai</div>
            <div class="item scr item00 l07"> <input id="pushUp" type="number"   name="pushUp"  required/></div>
            <div class="item scr item1 l07">
                <select   name="pushUp_Note" id="pushUp_Note">
                    @foreach ($notes as $note)
                        <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                    @endforeach
                </select>
            </div>

            <div class="item nscr item02 l08 r">Atsilenkimai</div>
            <div class="item scr item00 l08">  <input id="sitUp" type="number"   name="sitUp" required/></div>
            <div class="item scr item1 l08">
                <select   name="sitUp_Note" id="sitUp_Note">
                    @foreach ($notes as $note)
                        <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                    @endforeach
                </select>
            </div>

            <div class="item nscr item02 l09 r">Plank (lenta)</div>
            <div class="item scr item00 l09"> <input id="plank" type="number"   name="plank"  required/></div>
            <div class="item scr item1 l09">
                <select   name="plank_Note" id="plank_Note">
                    @foreach ($notes as $note)
                        <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                    @endforeach
                </select>
            </div>

            <div class="item nscr item02 l10 r">Nugaros atkėlimas su atrama</div>
            <div class="item scr item00 l10">  <input id="back" type="number"   name="back"  required/></div>
            <div class="item scr item1 l10">
                <select   name="back_Note" id="back_Note">
                    @foreach ($notes as $note)
                        <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                    @endforeach
                </select>
            </div>


            <div class="item nscr item02 l11 r">Pečių mobilumo testas</div>
            <div class="item scr item1 l11"> <input id="shoulders_L" type="number"   name="shoulders_L"  required/></div>
            <div class="item scr item1 l11"> <input id="shoulders_R" type="number"   name="shoulders_R" required /></div>
            <div class="item scr item1 l11">
                <select   name="shoulders_Note" id="shoulders_Note">
                    @foreach ($notes as $note)
                        <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                    @endforeach
                </select>
            </div>
            <div class="item nscr item02 l12 r">Šuolis į auktšti iš vietos</div>
            <div class="item scr item00 l12"> <input id="jump" type="number"   name="jump"  required/></div>
            <div class="item scr item1 l12">
                <select   name="jump_Note" id="jump_Note">
                    @foreach ($notes as $note)
                        <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                    @endforeach
                </select>
            </div>

            <div class="item nscr item02 l13 r">SSD Atsigavimas</div>
            <div class="item scr item00 l13"> <input id="hr" type="number"   name="hr"  required/> </div>
            <div class="item scr item1 l13">
                <select   name="hr_Note" id="hr_Note">
                    @foreach ($notes as $note)
                        <option contenteditable="false"  value="{{$note}}"> {{$note}}</option>
                    @endforeach
                </select>
            </div>


            <div class="item nscr item02 l14 r">Komentarai</div>
            <div class="item scr item00 l14"> <input id="comments" type="number"   name="comments"   required/> </div>
            <div class="item scr item1 l14"> <input id="comments_Note" type="text"   name="comments_Note" required  />
            </div>


            <div class="item nscr item02 h"></div>
            <div class="item scr item00 h"> </div>
            <div class=" hh"> <button type="submit" name="submit" value="submit" class="btn ">Įrašyti</button></div>

        </div>
        </form>
    </div>
</section>

<script>
    $("#date").datepicker( $.datepicker.regional[ "lt" ] );

    document.addEventListener("DOMContentLoaded", function() {
        var elements = document.getElementsByTagName("INPUT");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Laukas negali būti tuščias");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    })
</script>


<section class="profile-tbl">
    <div class="container profile">
        <form  >
            <div class="tab2">
                {{ Form::open() }}

                <div class="item nscr item01"> </div>

                <div class="item scr item1r r">Rezultatas</div>
                <div class="item scr item1 r">Pastaba</div>

                <div class="item nscr item01 l01 r">Data</div>


                <div class="item scr itemx3 l01 show">
                    <select class="form-control" name="records" id="records">
                        <option value="0" disable="true" selected="true">Pasirinkite datą</option>
                        @foreach ($records as $key => $value)
                            <option value="{{$value->date}}">{{ $value->date }}</option>
                        @endforeach
                    </select></div>

                {{ Form::close() }}

                <div class="item nscr item02 l02 r">Svoris</div>


                <div class="item scr item1r l02 show">
                    <a class="form-control" name="aweight" id="aweight">
                    </a>
                </div>

                <div class="item scr item1 l02 show">
                    <a class="form-control" name="aweight_Note" id="aweight_Note" >
                    </a>
                </div>

                <div class="item nscr itemy  yl03 r">Y </br> Balan</br>savimas</div>
                <div class="item nscr item1 l03"> </div>

                <div class="item scr item1 l03 r">Kairė </div>
                <div class="item scr item1 l03 r">Dešinė </div>
                <div class="item scr item1 l03"  > </div>

                <div class="item nscr item1 l04 r">Tiesiai</div>

                <div class="item scr item1 show l04">
                    <a class="form-control" name="ayForward_L" id="ayForward_L">
                    </a></div>
                <div class="item scr item1 show l04">
                    <a class="form-control" name="ayForward_R" id="ayForward_R">
                    </a></div>
                <div class="item scr item1 show l04">
                    <a class="form-control" name="ayForward_Note" id="ayForward_Note">
                    </a>
                </div>


                <div class="item nscr item1 l05 r">Į šoną tiesti, koja atgal</div>

                <div class="item scr item1 show l05">
                    <a class="form-control" name="aySide_L" id="aySide_L">
                    </a></div>
                <div class="item scr item1 show l05">
                    <a class="form-control" name="aySide_R" id="aySide_R">
                    </a></div>
                <div class="item scr item1 show l05">
                    <a class="form-control" name="aySide_Note" id="aySide_Note">
                    </a></div>


                <div class="item nscr item1 l06 r">Užsukta koja atgal</div>

                <div class="item scr item1 show l06">
                    <a class="form-control" name="ayBack_L" id="ayBack_L">
                    </a></div>
                <div class="item scr item1 show l06">
                    <a class="form-control" name="ayBack_R" id="ayBack_R">
                    </a></div>
                <div class="item scr item1 show l06">
                    <a class="form-control" name="ayBack_Note" id="ayBack_Note">
                    </a></div>


                <div class="item nscr item02 l07 r">Atsispaudimai</div>

                <div class="item scr item00 show l07">
                    <a class="form-control" name="apushUp" id="apushUp">
                    </a></div>
                <div class="item scr item1 show l07">
                    <a class="form-control" name="apushUp_Note" id="apushUp_Note">
                    </a></div>


                <div class="item nscr item02 l08 r">Atsilenkimai</div>

                <div class="item scr item00 show l08">
                    <a class="form-control" name="asitUp" id="asitUp">
                    </a>
                </div>
                <div class="item scr item1 show l08">
                    <a class="form-control" name="asitUp_Note" id="asitUp_Note">
                    </a>
                </div>



                <div class="item nscr item02 l09 r">Plank (lenta)</div>

                <div class="item scr item00 show l09">
                    <a class="form-control" name="aplank" id="aplank">
                    </a>
                </div>
                <div class="item scr item1 show l09">
                    <a class="form-control" name="aplank_Note" id="aplank_Note">
                    </a>
                </div>



                <div class="item nscr item02 l10 r">Nugaros atkėlimas su atrama</div>

                <div class="item scr item00 show l10">
                    <a class="form-control" name="aback" id="aback">
                    </a></div>
                <div class="item scr item1 show l10">
                    <a class="form-control" name="aback_Note" id="aback_Note">
                    </a></div>



                <div class="item nscr item02 l11 r">Pečių mobilumo testas</div>

                <div class="item scr item1 show l11">
                    <a class="form-control" name="ashoulders_L" id="ashoulders_L">
                    </a></div>
                <div class="item scr item1 show l11">
                    <a class="form-control" name="ashoulders_R" id="ashoulders_R">
                    </a></div>
                <div class="item scr item1 show l11">
                    <a class="form-control" name="ashoulders_Note" id="ashoulders_Note">
                    </a></div>


                <div class="item nscr item02 l12 r">Šuolis į auktšti iš vietos</div>

                <div class="item scr item00 show l12">
                    <a class="form-control" name="ajump" id="ajump">
                    </a></div>
                <div class="item scr item1 show l12">
                    <a class="form-control" name="ajump_Note" id="ajump_Note">
                    </a></div>


                <div class="item nscr item02 l13 r">SSD Atsigavimas</div>

                <div class="item scr item00 show l13">
                    <a class="form-control" name="ahr" id="ahr">
                    </a></div>
                <div class="item scr item1 show l13">
                    <a class="form-control" name="ahr_Note" id="ahr_Note">
                    </a></div>


                <div class="item nscr item02 l14 r">Komentarai</div>

                <div class="item scr item00 show l14">
                    <a class="form-control" name="acomments" id="acomments">
                    </a></div>
                <div class="item scr item1 show l14">
                    <a class="form-control" name="acomments_Note" id="acomments_Note">
                    </a>
                </div>



                <div class="item nscr item02 h"> </div>
                <div class="item scr item00 h"> </div>
            </div>

        </form>
    </div>
</section>


<script type="text/javascript">
    $('#records').on('change', function(e){
        console.log(e);
        var date_id = e.target.value;
        $.get('/json-date?date_id=' + date_id,function(data) {
            console.log(data);
            @foreach ($pro as $key )
            $('#a{{$key}}').empty();
            @endforeach

            $.each(data, function(index, datesObj){
                @foreach ($pro  as $key )

                $('#a{{$key}}').append('<a'+ datesObj.id +'">'+ datesObj.{{$key}} +'</a>');

                @endforeach

            })
        });
    });


</script>







