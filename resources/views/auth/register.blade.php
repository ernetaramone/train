@extends('layouts.reg')

@section('content_reg')
<section class="reg-s">
<div class="container reg-f">
    <div class="row justify-content-center row-centered reg2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h3>Registracija</h3></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Vardas</label>

                            <div class="col-md-6">
                                <input id="name" type="text" placeholder="Jonas" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="surname" class="col-md-4 col-form-label text-md-right">Pavardė</label>

                            <div class="col-md-6">
                                <input id="surname" type="text" placeholder="Jonaitis" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname') }}" required autofocus>

                                @if ($errors->has('surname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sex" class="col-md-4 col-form-label text-md-right">Lytis</label>

                            <div class="col-md-6">

                                <select class="form-control" name="sex" id="sex">
                                    @foreach ($vm = array('Vyras', 'Moteris')  as $sex)
                                        <option contenteditable="false" value="{{$sex}}"> {{$sex}}</option>
                                    @endforeach


                                </select>



                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="age" class="col-md-4 col-form-label text-md-right">Amžius</label>

                            <div class="col-md-6">
                                <input id="age" type="text" placeholder="24" class="form-control{{ $errors->has('age') ? ' is-invalid' : '' }}" name="age" value="{{ old('age') }}" required>

                                @if ($errors->has('age'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('age') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="height" class="col-md-4 col-form-label text-md-right">Ūgis</label>

                            <div class="col-md-6  ">
                                    <select class="form-control " id="height" name="height" contenteditable="false">
                                        @foreach (range(120, 230) as $height)
                                        <option contenteditable="false" value="{{$height}}">{{$height}} </option>
                                        @endforeach

                                    </select>


                        </div>
                            </div>

                        <div class="form-group row">
                            <label for="goal" class="col-md-4 col-form-label text-md-right">Jūsų tikslas</label>

                            <div class="col-md-6">
                                <select class="form-control" id="goal" name="goal">
                                    <?php $goals= array('Numesti svorio', 'Priaugti mases', 'Sustipreti', 'Padailinti kuno linijas'); ?>
                                    @foreach ($goals as $goal)
                                        <option  contenteditable="false" value="{{$goal}}">{{$goal}}</option>

                                        @endforeach


                                </select>



                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">El-paštas</label>

                            <div class="col-md-6">
                                <input id="email" type="email" placeholder="jonas@gmail.com" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Slaptažodis</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Patvirtinkite slaptažodį</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registruotis') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<script>

    document.addEventListener("DOMContentLoaded", function() {
        var elements = document.getElementsByTagName("INPUT");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Laukas negali būti tuščias");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    })
</script>
@endsection
