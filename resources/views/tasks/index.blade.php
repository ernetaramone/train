<section class="container sec-cal">
    <h3>Užsiėmimų kalendorius</h3>
    <div id='calendar'></div>

</section>

<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script src='fullcalendar/locale/lt.js'></script>
<script>
    $(document).ready(function  showCalendar() {
        $('#calendar').fullCalendar({
            locale: 'lt',
            axisFormat: 'HH:mm',
            timeFormat: 'HH:mm',
            minTime: 0,
            maxTime: 24,

            events : [
                    @foreach($tasks as $task)
                {
                    title : '{{ $task->workout  }}' ,
                    start : '{{ $task->task_date }}',
                    className: '{{ $task->user_id }}',
                    id:'{{ $task->ver }}',

                },       @endforeach
            ],

            eventRender:
                @foreach($id as $dd)

                function(event, element) {
                if(event.className == '{{ $dd->id }}' && event.id == '{{ $dd->ver }}')
                 {
                    element.css({
                        'background-color': '#16279e',
                        'border-color': '#16279e'
                    });


                 }
                else if (event.className == '{{ $dd->id }}' && event.id != '{{ $dd->ver }}'){
                    element.css({
                        'background-color': '#008700',
                        'border-color': '#008700'
                    });
                }
                else if (event.className != '{{ $dd->id }}' && event.id == '{{ $dd->ver }}'){
                    element.css({
                        'background-color': '#ffbd7e',
                        'border-color': '#ffbd7e'
                    });
                }
                    else
                      {
                        element.css({
                            'background-color': '#ff2c27',
                            'border-color': '#ff2c27'
                        });
                    }
            }, @endforeach



        })
    });

</script>
