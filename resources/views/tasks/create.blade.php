<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- Minified JS library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Minified Bootstrap JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src='js//locales/bootstrap-datetimepicker.lt.js' type='text/javascript'></script>
<link rel="stylesheet" href="{{ asset('css/style.css') }}">

<section class="container sec-top-che sec-top sec-cal">
    <form action="{{ route('tasks.store') }}" method="post">
        {{ csrf_field() }}

        <div class="form-group row">
            <label for="workout" class="col-md-4 col-form-label text-md-right">Treniruotė</label>
            <div class="col-md-4">
                <select   name="workout" id="workout">
                    @foreach ($workouts = array('Kūno dizainas', 'Svorio metimas', 'Core', 'Cardio')  as $workout)
                        <option contenteditable="false"  value="{{$workout}}"> {{$workout}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="task_date" class="col-md-4 col-form-label text-md-right">Treniruotės data ir laikas:</label>

            <div class="col-md-4">
                <input  name="task_date" class="datetime2 "id="datetime2" contenteditable="false" required />
                <button type="submit"  id="button" class="btn btn-dark">Saugoti</button>
            </div>
        </div>

            <button  id="button" class="btn"  disabled style=" background: #008700; color: white; opacity: 1;"> Jūsų patvirtinta treniruotė</button>
            <button  id="button" class="btn"  disabled style=" background: #1e429e;  color: white; opacity: 1;">Jūsų nepatvirtinta treniruotė</button>
            <button  id="button" class="btn"  disabled style=" background: #ff2c27; opacity: 1;">Patvirtinta treniruotė</button>
            <button  id="button" class="btn" disabled style=" background: #ffbd7e; opacity: 1;">Nepatvirtinta treniruotė</button>


    </form>
    <hr>
</section>

    <script type="text/javascript">
        $("#datetime2").datetimepicker({
            language: 'lt',
            startDate: new Date(),
            format: 'yyyy-mm-dd hh:ii',
            autoclose: true,
        });

        document.addEventListener("DOMContentLoaded", function() {
            var elements = document.getElementsByTagName("INPUT");
            for (var i = 0; i < elements.length; i++) {
                elements[i].oninvalid = function(e) {
                    e.target.setCustomValidity("");
                    if (!e.target.validity.valid) {
                        e.target.setCustomValidity("Laukas negali būti tuščias");
                    }
                };
                elements[i].oninput = function(e) {
                    e.target.setCustomValidity("");
                };
            }
        })
    </script>


