<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurements', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('user_id');
            $table->integer('weight');
            $table->string('weight_Note');
            $table->integer('yForward_L');
            $table->integer('yForward_R');
            $table->string('yForward_Note');
            $table->integer('ySide_L');
            $table->integer('ySide_R');
            $table->string('ySide_Note');
            $table->integer('yBack_L');
            $table->integer('yBack_R');
            $table->string('yBack_Note');
            $table->integer('pushUp');
            $table->string('pushUp_Note');
            $table->integer('sitUp');
            $table->string('sitUp_Note');
            $table->integer('plank');
            $table->string('plank_Note');
            $table->integer('back');
            $table->string('back_Note');
            $table->integer('shoulders_L');
            $table->integer('shoulders_R');
            $table->string('shoulders_Note');
            $table->integer('jump');
            $table->string('jump_Note');
            $table->integer('hr');
            $table->string('hr_Note');
            $table->string('comments');
            $table->string('comments_Note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurements');
    }
}
